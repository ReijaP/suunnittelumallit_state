
public class Pokemon {
	
	private PokemonState state;
	
	public Pokemon() {
		state = new Charmander();
	}
	
	public void attack() {
		state.attack(this);
	}
	
	public void move() {
		state.move(this);
	}
	
	public void introduce() {
		state.introduce();
	}
	
	protected void changeState(PokemonState ps) {
		state = ps;
	}

}

import java.util.Random;

public abstract class PokemonState {
	
	public void attack(Pokemon p) {}
	public void introduce(){}
	
	public void move(Pokemon p) {}
	
	public void changeState(Pokemon p, PokemonState s) {
		p.changeState(s);
	}


}

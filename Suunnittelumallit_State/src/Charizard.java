import java.util.Random;

public class Charizard extends PokemonState{
	
	public Charizard() {
		Introduce();
	}
	
	public void attack(Pokemon p) {
		System.out.println("Charizard tekee ilmaiskun!");
		Random rand = new Random();
		int success = rand.nextInt(100);
		
		if(success >= 50) {
			System.out.println("Ilmaisku osui vastustajaan!");
		}else {
			System.out.println("Ilmaisku jäi lyhyeksi!");
			p.changeState(new Charmeleon());
		}
	}
	
	public void move(Pokemon p) {
		Random rand = new Random();
		int direction = rand.nextInt(100);
		
		if(direction < 50){
			System.out.println("Charizard lensi vasemmalle!");
		}else {
			System.out.println("Charizard lensi oikealle!");
		}
	}
	
	public void Introduce() {
		System.out.println("\t\n Valitsen sinut Charizard!\n");
	}

}

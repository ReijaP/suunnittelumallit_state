import java.util.Random;

public class Charmeleon extends PokemonState {
	
	public Charmeleon() {
		Introduce();
	}
	
	public void attack(Pokemon p) {
		System.out.println("Charmeleon lähettää tulipallon!");
		Random rand = new Random();
		int success = rand.nextInt(100);
		
		if(success >= 50) {
			System.out.println("Tulipallo osui kohteeseen!!");
			p.changeState(new Charizard());
		}else {
			System.out.println("Tulipallo lensi kohteen ohi!");
			p.changeState(new Charmander());
		}
	}
	
	public void move(Pokemon p) {
		Random rand = new Random();
		int direction = rand.nextInt(100);
		
		if(direction < 50){
			System.out.println("Charmeleon hyppäsi vasemmalle!");
		}else {
			System.out.println("Charmeleon hyppäsi oikealle!");
		}
	}
	
	public void Introduce() {
		System.out.println("\t\n Valitsen sinut Charmeleon!\n");
	}


}

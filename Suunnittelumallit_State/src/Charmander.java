import java.util.Random;

public class Charmander extends PokemonState {
	
	public Charmander() {
		Introduce();
	}

	public void attack(Pokemon p) {
		System.out.println("Charmander tekee tuli-iskun!");
		Random rand = new Random();
		int success = rand.nextInt(100);
		
		if(success >= 50) {
			System.out.println("Tuli-isku onnistui!");
			p.changeState(new Charmeleon());
		}else {
			System.out.println("Isku epäonnistui!");
		}
	}
	
	public void move(Pokemon p) {
		Random rand = new Random();
		int direction = rand.nextInt(100);
		
		if(direction < 50){
			System.out.println("Charmander väisti vasemmalle!");
		}else {
			System.out.println("Charmander väisti oikealle!");
		}
	}
	
	public void Introduce() {
		System.out.println("\t\n Valitsen sinut Charmander!\n");
	}
}
